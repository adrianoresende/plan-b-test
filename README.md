# Test Front-end

## Version

npm 3.10.10

vue 2.5.2

## Configuração

``` bash
# Instalar modules
npm install

# Em desenvolvimento
npm run dev

# Em produção
npm run build
cd dist
python -m SimpleHTTPServer 8000 (terminal - unix ou osmac)
py -m http.server 8000 (powershell - windows)

```
