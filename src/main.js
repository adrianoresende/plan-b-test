import 'normalize.css'

import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

// Components registration
// import * as Components from './components/**/*'
// Object.values(Components).map(component =>
//   component.name && Vue.component(component.name, component)
// )

// Print Author in Console Developer
const style = [
  'background: linear-gradient(45deg, #e96815 0%, #2b1100 100%);',
  'color: white',
  'padding: 24px 26px',
  'font-size: 12px',
  'line-height: 68px',
].join(';');
const textConsole = '%c Plan-b Test | Coded by AdrianoResende.com.br (adrianorbs@gmail.com) ;D'
console.log(textConsole, style);

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
